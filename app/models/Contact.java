package models;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import exceptions.EmptyParametersException;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mongodb.morphia.annotations.Entity;
import play.data.validation.Email;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.modules.morphia.Model;
import play.modules.morphia.Model.NoAutoTimestamp;
import play.modules.morphia.validation.Unique;

import java.security.InvalidParameterException;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@NoAutoTimestamp
@Entity
public class Contact extends Model {
    @Required
    public String information;

    @Required
    public String name;

    public String company;

    public String profileImage;

    @Required
    @Unique
    @Email
    public String email;

    @Required
    public Date birthdate;

    @Phone
    public String workPhoneNumber;

    @Required
    @Unique
    @Phone
    public String personalPhoneNumber;

    @Required
    public Address address;

    public static class Address {
        @Required
        String direction;
        @Required
        String zipCode;
        @Required
        String city;
        @Required
        String state;
        @Required
        String country;
    }

    public Contact(JsonObject jsonObjectRequest) {
        address = new Address();
        setFieldsFrom(jsonObjectRequest);
    }

    public static Contact findByEmailOrPhone(String email, String phone) throws EmptyParametersException {
        MorphiaQuery query = Contact.q();

        if (Strings.isNullOrEmpty(email) && Strings.isNullOrEmpty(phone)) {
            throw new EmptyParametersException("You must define at least one parameter.");
        }

        if (email != null){
            query.and(
                query.criteria("email").equal(Pattern.compile(email, Pattern.CASE_INSENSITIVE))
            );
        }

        if (phone != null){
            query.and(
                query.criteria("personalPhoneNumber").equal(phone)
            );
        }

        return query.get();
    }

    public static List<Contact> findByStateOrCity(String state, String city) throws EmptyParametersException {
        MorphiaQuery query = Contact.q();

        if (Strings.isNullOrEmpty(state) && Strings.isNullOrEmpty(city)) {
            throw new EmptyParametersException("You must define at least one parameter.");
        }

        if (state != null){
            query.and(
                query.criteria("address.state").equal(Pattern.compile(state, Pattern.CASE_INSENSITIVE))
            );
        }

        if (city != null){
            query.and(
                query.criteria("address.city").equal(Pattern.compile(city, Pattern.CASE_INSENSITIVE))
            );
        }

        return query.asList();
    }

    public void setFieldsFrom(JsonObject jsonObjectRequest) {
        if (jsonObjectRequest.get("information") != null) {
            this.information = jsonObjectRequest.get("information").getAsString();
        }

        if (jsonObjectRequest.get("name") != null) {
            this.name = jsonObjectRequest.get("name").getAsString();
        }

        if (jsonObjectRequest.get("company") != null) {
            this.company = jsonObjectRequest.get("company").getAsString();
        }

        if (jsonObjectRequest.get("profileImage") != null) {
            this.profileImage = jsonObjectRequest.get("profileImage").getAsString();
        }

        if (jsonObjectRequest.get("email") != null) {
            this.email = jsonObjectRequest.get("email").getAsString();
        }

        if (jsonObjectRequest.get("birthdate") != null) {
            DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
            this.birthdate = dtf.parseDateTime(jsonObjectRequest.get("birthdate").getAsString()).toDate();
        }

        if (jsonObjectRequest.get("workPhoneNumber") != null) {
            this.workPhoneNumber = jsonObjectRequest.get("workPhoneNumber").getAsString();
        }

        if (jsonObjectRequest.get("personalPhoneNumber") != null) {
            this.personalPhoneNumber = jsonObjectRequest.get("personalPhoneNumber").getAsString();
        }

        if (jsonObjectRequest.get("address") != null) {
            loadAddress(jsonObjectRequest);
        }
    }

    private void loadAddress(JsonObject jsonObjectRequest) {
        if (jsonObjectRequest.get("address") != null) {
            JsonObject addressJsonObject = jsonObjectRequest.get("address").getAsJsonObject();

            if (addressJsonObject.get("direction") != null) {
                this.address.direction = addressJsonObject.get("direction").getAsString();
            }

            if (addressJsonObject.get("zipCode") != null) {
                this.address.zipCode = addressJsonObject.get("zipCode").getAsString();
            }

            if (addressJsonObject.get("city") != null) {
                this.address.city = addressJsonObject.get("city").getAsString();
            }

            if (addressJsonObject.get("state") != null) {
                this.address.state = addressJsonObject.get("state").getAsString();
            }

            if (addressJsonObject.get("country") != null) {
                this.address.country = addressJsonObject.get("country").getAsString();
            }
        }
    }

    public String toString() {
        return "{" +
                "   \"information\":" + this.information +", " +
                "   \"name\":" + this.name +", " +
                "   \"company\":" + this.company +", " +
                "   \"profileImage\":"+this.profileImage+", " +
                "   \"email\":" + this.email +", " +
                "   \"birthdate\":" + this.birthdate +", " +
                "   \"workPhoneNumber\":" + this.workPhoneNumber +", " +
                "   \"personalPhoneNumber\":" + this.personalPhoneNumber +", " +
                "   \"address\":{  " +
                "       \"direction\":" + this.address.direction +", " +
                "       \"zipCode\":" + this.address.zipCode +", " +
                "       \"city\":" + this.address.city +", " +
                "       \"state\":" + this.address.state +", " +
                "       \"country\":" + this.address.country +", " +
                "   }" +
                "}";
        
    }
}
