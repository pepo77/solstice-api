package controllers;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import exceptions.EmptyParametersException;
import models.Contact;
import org.bson.types.ObjectId;

import java.util.List;

public class Contacts extends BaseSecureController {

    public static void create(String body) {
        JsonObject jsonObjectRequest = new JsonParser().parse(body).getAsJsonObject();
        Contact contact = new Contact(jsonObjectRequest);
        if (!contact.validateAndCreate()) {
            badRequest("Some fields are not properly formatted.");
        } else {
            renderText("{ id: " + contact.getIdAsStr() + "}");
        }
    }

    public static void delete(String contactId) {
        Contact contact = Contact.findById(new ObjectId(contactId));

        if (contact != null) {
            contact.delete();
        }
    }

    public static void update(String contactId, String body) {
        JsonObject jsonObjectRequest = new JsonParser().parse(body).getAsJsonObject();
        Contact contact = Contact.findById(new ObjectId(contactId));
        contact.setFieldsFrom(jsonObjectRequest);

        if (!contact.validateAndSave()) {
            badRequest("Some fields are not properly formatted.");
        }
    }

    public static void get(String contactId) {
        Contact contact = Contact.findById(new ObjectId(contactId));
        renderJSON(contact);
    }

    public static void searchByEmailOrPhone(String email, String phone) {
        try {
            Contact contact = Contact.findByEmailOrPhone(email, phone);
            if (contact != null) {
                JsonElement jsonElement = (new Gson()).toJsonTree(contact);
                jsonElement.getAsJsonObject().remove("_id");
                jsonElement.getAsJsonObject().remove("__blobs");
                jsonElement.getAsJsonObject().addProperty("_id", contact.getIdAsStr());

                renderJSON(jsonElement);
            } else {
                renderJSON(contact);
            }
        } catch (EmptyParametersException e) {
            forbidden(e.getMessage());
        }
    }

    public static void retrieveAllByStateOrCity(String state, String city) {
        try {
            List<Contact> contacts = Contact.findByStateOrCity(state, city);
            List<JsonElement> jsonElementList = Lists.newArrayList();

            for (Contact contact : contacts){
                if (contact != null) {
                    JsonElement jsonElement = (new Gson()).toJsonTree(contact);
                    jsonElement.getAsJsonObject().remove("_id");
                    jsonElement.getAsJsonObject().remove("__blobs");
                    jsonElement.getAsJsonObject().addProperty("_id", contact.getIdAsStr());
                    jsonElementList.add(jsonElement);
                }
            }

            renderJSON(jsonElementList);
        } catch (Exception e) {
            forbidden(e.getMessage());
        }
    }

    public static void retrieveAll() {
        try {
            List<Contact> contacts = Contact.findAll();
            List<JsonElement> jsonElementList = Lists.newArrayList();

            for (Contact contact : contacts){
                if (contact != null) {
                    JsonElement jsonElement = (new Gson()).toJsonTree(contact);
                    jsonElement.getAsJsonObject().remove("_id");
                    jsonElement.getAsJsonObject().remove("__blobs");
                    jsonElement.getAsJsonObject().addProperty("_id", contact.getIdAsStr());
                    jsonElementList.add(jsonElement);
                }
            }

            renderJSON(jsonElementList);
        } catch (Exception e) {
            forbidden(e.getMessage());
        }
    }
}
