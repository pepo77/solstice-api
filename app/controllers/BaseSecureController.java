package controllers;

import play.Play;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Http;

/**
 * Implement basic HTTP Authorization methods for controllers.
 */
public class BaseSecureController extends Controller {

	private static final String API_USER = Play.configuration.getProperty("api.user");
	private static final String API_PASSWORD = Play.configuration.getProperty("api.password");

	@Before
	public static void myHTTPBasicAuth() {
		boolean authorized = false;
		Http.Header header = request.headers.get("authorization");

		if (header != null) {
			if (API_PASSWORD.equals(request.password) && API_USER.equals(request.user)) {
				authorized = true;
			} else {
				forbidden();
			}
		}
		if (!authorized) {
			unauthorized(Messages.get("Enter a username and password"));
		}
	}
}
