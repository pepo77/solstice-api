package exceptions;

public class EmptyParametersException extends Exception {
    public EmptyParametersException(String s) {
        super(s);
    }
}
