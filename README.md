## Solstice's Contacts API

### About the implementation

The api was developed in Java, using Playframework 1.4.4, with MongoDB to persist the data and Heroku to make the 
public deploy. It is also deployable in other platforms, like AWS, but we make the deploy in this way for simplicity.

In the next sections we will explain how to deploy it in this environment, and how use it.

### Deploy

The api is developed with Java, with Playframework 1.4.4, and is deployed in a Heroku server, i.e.:
https://solstice-api-aee.herokuapp.com/

We will explain how to make the deploy in this platform.

#### Prerequisites

* Have installed git (how to [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)).
* Have installed Heroku CLI (how to [here](https://devcenter.heroku.com/articles/heroku-cli)).

#### Steps

1. First of all is necessary to have a heroku account, and create an application to store the api. It must be a Java 
application.
2. Unzip the project folder.
3. Open a terminal into the project folder path (*).
4. Run `git init` (*).
5. Add the git url as a remote with `git remote add heroku https://git.heroku.com/solstice-api-aee.git` (I used my 
heroku app's url as example).   
6. Run `heroku buildpacks:set https://github.com/heroku/heroku-buildpack-play
` to set the buildpack for Play 1.x in Heroku.
7. Run `git push heroku master` to make the deploy into the heroku server.

(*) Or contact me via email to `aeeisner@yahoo.com.ar`, send me a valid bitbucket account, and I will give you 
access to the repository.

#### Important data

* In the application.conf file are all the info to get connected to the database.
It is a mongodb, and can be accessed using some MongoDB GUI like Studio3t.
* The java version is 1.8. It is defined in the system.properties file.


### Use

In the following paragraphs, we will to learn how to use the Solstice's Contacts API.
The mentioned API allows us to do CRUD operations over a Contact with the following structure:

    {
        "information": "some info",
        "name": "JOHN SMITH",
        "company": "SMALLSYS INC",
        "profileImage": "",
        "email": "johnsmith@fmc.com",
        "birthdate": "1977-04-08",
        "workPhoneNumber": "(305) 613 09 58 ext 101",
        "personalPhoneNumber": "(305) 613 09 59",
        "address": {
            "direction": "2050 Bamako Place",
            "zipCode": "20521-2050",
            "city": "Washington",
            "state": "DC",
            "country": "USA"
        }
    }
    
Also you can do some queries, that are explained in the following sections.

#### Required fields

Is not possible create a Contact without these fields.

    * information
    * name
    * email
    * birthdate
    * personalPhoneNumber
    * address:
        * direction
        * zipCode
        * city
        * state
        * country
        
Also, the fields `email` and `personalPhoneNumber` cannot have duplicated values.

#### Special formats

The following fields must have specific formats.

* __birthdate__: yyyy-MM-dd
* __workPhoneNumber__ and __personalPhoneNumber__: uses the format of phone explained [here](https://www.playframework.com/documentation/1.4.x/validation-builtin#phone)
* __email__: must be a valid email.

#### Other comments

* The profileImage stores a string with the url to a image stored in an image hosting site.

---

#### Operations

**Create a new Contact**

To create a new contact, send a request like the following. It will create a new contact,
and return the id of the created entity.

Type of request:

    POST

Endpoint: 
    
    https://solstice-api-aee.herokuapp.com/contact

Data:

    json with the pairs field:value to update, taking care of the mandatory fields and the special formats specified 
    above

Example request with cURL:

    curl --request POST \
      --url https://solstice-api-aee.herokuapp.com/contact \
      --header 'authorization: Basic c29sc3RpY2UtYXBpLXVzZXI6c29sc3RpY2UtYXBpLXBhc3N3b3Jk' \
      --header 'content-type: application/json' \
      --cookie PLAY_ERRORS= \
      --data '{
    	"information": "some info",
    	"name": "JOHN SMITH",
    	"company": "SMALLSYS INC",
    	"profileImage": "",
    	"email": "johnsmith@fmc.com",
    	"birthdate": "1977-04-08",
    	"workPhoneNumber": "(305) 613 09 58 ext 101",
    	"personalPhoneNumber": "(305) 613 09 59",
    	"address": {
                "direction": "2050 Bamako Place",
                "zipCode": "20521-2050",
                "city": "Washington",
                "state": "DC",
                "country": "USA"
    	}
    }'

**Get an existing Contact**

This query returns an existing contact, having received the id of the contact.

Type of request:

    GET

Endpoint: 
    
    https://solstice-api-aee.herokuapp.com/contact/{id}

Url Parameters: 

    id: id of the document in the database.
    
Example request with cURL:

    curl --request GET \
      --url https://solstice-api-aee.herokuapp.com/contact/5abb9ab493f4829f5c756168 \
      --header 'authorization: Basic c29sc3RpY2UtYXBpLXVzZXI6c29sc3RpY2UtYXBpLXBhc3N3b3Jk' \
      --header 'content-type: application/json' \
      --cookie PLAY_ERRORS=

---

**Update an existing Contact**

The update can be for any field of the Contact.
The following example updates the Contact's personalPhoneNumber field.

Type of request:

    PUT

Endpoint: 
    
    https://solstice-api-aee.herokuapp.com/contact/{id}

Url Parameters: 

    id: id of the document in the database.
    
Data:

    json with the pairs field:value to update.

Example request with cURL:


with cURL:

    curl --request PUT \
      --url https://solstice-api-aee.herokuapp.com/contact/5abb9ab493f4829f5c756168 \
      --header 'authorization: Basic c29sc3RpY2UtYXBpLXVzZXI6c29sc3RpY2UtYXBpLXBhc3N3b3Jk' \
      --header 'content-type: application/json' \
      --cookie PLAY_ERRORS= \
      --data '{
        "personalPhoneNumber": "(305) 613 09 59"
    }'

---

**Delete an existing Contact**

Type of request:

    DELETE

Endpoint: 
    
    https://solstice-api-aee.herokuapp.com/contact/{id}

Parameters: 

    id: id of the document in the database.

Example request with cURL:

    curl --request DELETE \
      --url https://solstice-api-aee.herokuapp.com/contact/5aba8e6593f4829f5c756167 \
      --header 'authorization: Basic c29sc3RpY2UtYXBpLXVzZXI6c29sc3RpY2UtYXBpLXBhc3N3b3Jk' \
      --header 'content-type: application/json' \
      --cookie PLAY_ERRORS=

---

**Retrieve all Contacts**

Retrieve all the Contacts stored in the database.

Type of request:

    GET
    
Endpoints: 
    
    https://solstice-api-aee.herokuapp.com/contacts
    
Example request with cURL:

    curl --request GET \
      --url 'https://solstice-api-aee.herokuapp.com/contacts' \
      --header 'authorization: Basic c29sc3RpY2UtYXBpLXVzZXI6c29sc3RpY2UtYXBpLXBhc3N3b3Jk' \
      --header 'content-type: application/json'

---

**Search for a Contact by email or phone number**

Retrieve the Contact that have the email or phone number specified. The is considered only the personal phone number 
to the search. If both parameter are specified, both restrictions are applied over all the results. Is mandatory 
define at least one parameter.

Type of request:

    GET

Endpoint: 
    
    https://solstice-api-aee.herokuapp.com/contacts/contact-data

Parameters: 

    email
    phone

Example request with cURL:

    curl --request GET \
      --url 'https://solstice-api-aee.herokuapp.com/contacts/contact-data?email=johnsmith%40fmc.com&phone=' \
      --header 'authorization: Basic c29sc3RpY2UtYXBpLXVzZXI6c29sc3RpY2UtYXBpLXBhc3N3b3Jk' \
      --header 'content-type: application/json'

---

**Retrieve all Contacts from the same state or city**

Retrieve all the Contacts that live in the same state or city. If both parameter are specified, both restrictions are 
applied over all the results.

Type of request:

    GET
    
Endpoints: 
    
    https://solstice-api-aee.herokuapp.com/contacts/state/{state}/city/{city}
    https://solstice-api-aee.herokuapp.com/contacts/state/{state}
    https://solstice-api-aee.herokuapp.com/contacts/city/{city}

Parameters: 

    state
    city

Example request with cURL:

    curl --request GET \
      --url 'https://solstice-api-aee.herokuapp.com/contacts/state/DC/city/Washington' \
      --header 'authorization: Basic c29sc3RpY2UtYXBpLXVzZXI6c29sc3RpY2UtYXBpLXBhc3N3b3Jk' \
      --header 'content-type: application/json'

---

### Automated Tests

In order to test the basic lifecycle of a Contact entity, we written some unitary tests.

To run it, is necessary deploy locally the application, and run the app with the following command:

    play test
    
Then, accessing to `localhost:9000@tests` with a web browser. Then select the ApplicationTest an click in Start. All 
these steps are explained [here](https://www.playframework.com/documentation/1.4.x/guide10#controller).