import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.bson.types.ObjectId;
import org.junit.*;
import play.test.*;
import play.mvc.*;
import play.mvc.Http.*;
import models.*;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class ApplicationTest extends FunctionalTest {

    private String contactId;

    @Test
    public void completeLifeCycleTest() throws UnsupportedEncodingException {
        createTest();
        updateTest();
        deleteTest();
    }

    public void createTest() throws UnsupportedEncodingException {
        Request request = getInitializedRequest();

        String body = getInsertTestBody();

        Response response = POST(request,"/contact", "application/json", body);

        assertIsOk(response);

        byte[] data = response.out.toByteArray();
        String content = new String(data, response.encoding);
        JsonObject jsonObject = new JsonParser().parse(content).getAsJsonObject();

        assertTrue(jsonObject.get("id") != null);

        contactId = jsonObject.get("id").getAsString();
    }

    public void updateTest() throws UnsupportedEncodingException {
        Request request = getInitializedRequest();

        Map contact = Maps.newHashMap();
        contact.put("information", "test info changed");
        String body = new Gson().toJson(contact);
        Response response = PUT(request,"/contact/" + contactId, "application/json", body);

        assertIsOk(response);

        Contact updatedContact = Contact.findById(new ObjectId(contactId));

        assertTrue(updatedContact != null);
        assertTrue("test info changed".equals(updatedContact.information));
    }

    public void deleteTest(){
        Request request = getInitializedRequest();

        Contact undeletedContact = Contact.findById(new ObjectId(contactId));

        assertTrue(undeletedContact == null);

        Response response = DELETE(request,"/contact/" + contactId);
        assertIsOk(response);

        Contact deletedContact = Contact.findById(new ObjectId(contactId));

        assertTrue(deletedContact == null);
    }

    private String getInsertTestBody() {
        Map contact = Maps.newHashMap();
        Map address = Maps.newHashMap();

        contact.put("information", "test info");
        contact.put("name", "JOHN SMITH");
        contact.put("company", "SMALLSYS INC");
        contact.put("profileImage", "");
        contact.put("email", "johnsmith3@smallsys.com");
        contact.put("birthdate", "1977-04-08");
        contact.put("workPhoneNumber", "(305) 613 09 58 ext 101");
        contact.put("personalPhoneNumber", "(305) 613 09 61");

        address.put("direction", "2050 Bamako Place");
        address.put("zipCode", "20521-2050");
        address.put("city", "Washington");
        address.put("state", "DC");
        address.put("country", "USA");

        contact.put("address", address);

        return new Gson().toJson(contact);
    }

    private Request getInitializedRequest() {
        Request request;
        Header basicAuth;
        request = new Request();
        basicAuth = new Header("authorization", "Basic c29sc3RpY2UtYXBpLXVzZXI6c29sc3RpY2UtYXBpLXBhc3N3b3Jk");
        request.headers.put("authorization", basicAuth);
        request._init();
        return request;
    }

}